import pandas as pd
import os
from geopy.geocoders import Nominatim
import csv

############## Options ##############

pd.options.display.max_columns = None
pd.options.display.max_rows = None

#####################################

############ Variables ##############

path_db = '../data/db/registre-national-installation-production-stockage-electricite-agrege-311217.csv'

usecols = ['commune', 'codeDepartement', 'departement', 'region',
           'filiere', 'regime', 'gestionnaire',
           'energieAnnuelleInjectee']


#####################################

def get_coordinates(fd, address):
    r = csv.reader(fd, delimiter=",")

    # Check if the coordinates are already retrieved
    for row in r:
        if row[0] == address:
            print('***' + address + ' coordinates | found')
            return row[1], row[2]

    # Retrieve the coordinates and add them to the cache
    geolocator = Nominatim(user_agent="example app")
    data = geolocator.geocode(address)

    new_row = [address, None, None]
    if data != None:
        new_row = [address, data.point.longitude, data.point.latitude]

    w = csv.writer(fd, delimiter=",")
    w.writerow(new_row)
    print('***' + address + ' coordinates | added')

    return new_row[1], new_row[2]


if __name__ == '__main__':
    df = pd.read_csv(path_db, sep=';', encoding='UTF-8', usecols=usecols)

    print(df.filiere.unique())

    # Drop rows with NaN values
    df.dropna(inplace=True)

    # Filter the rows
    filiere_type = ["Hydraulique"]
    df = df[df.filiere.isin(filiere_type)]

    df = df[df.regime == 'En service']

    df = df[df.energieAnnuelleInjectee > 0]

    # Add longitude and latitude columns
    f_cache = '../data/res/coordinates_cache.csv'
    if not os.path.exists(f_cache):
        fd = open(f_cache, 'x')
        fd.close()

    fd = open(f_cache, 'r+', newline='', encoding="UTF-8")

    longitude = list()
    latitude = list()
    for addr in df['commune']:
        long, lat = get_coordinates(fd, addr)
        longitude.append(long)
        latitude.append(lat)

    fd.close()

    df['longitude'] = longitude
    df['latitude'] = latitude

    # Drop rows with no coordinates
    df = df[(df['longitude'] != '') & (df['latitude'] != '')]

    # Create a string who brings filiere_type together
    s_filiere = ''
    for elt in filiere_type:
        s_filiere += '_' + elt

    # Save the dataframe in a CSV file
    f_output = '../data/res/processed_data' +  s_filiere + '.csv'
    if (not os.path.exists(f_output)):
        print('*** creating file')
        df.to_csv(f_output, sep=';', encoding='UTF-8')
        print('*** file created')

    # Show data
    print("NB ROWS:", df.count())
    print(df.head(10))
    print(df.describe())
