import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# Print as many columns as we want
desired_width = 320
nb_columns = 25

pd.set_option('display.width', desired_width)
np.set_printoptions(linewidth=desired_width)
pd.set_option('display.max_columns', nb_columns)

# Load csv file
path_db = '../data/db/consommation-annuelle-brute-regionale.csv'
df = pd.read_csv(path_db, sep=';')

# Take data from 2018 (as 2019 are incomplete, need to work on that)
from_2020 = df[df['annee'] == 2020]
# Keep only cities that have at least two different energy sources (electricity + gas)
from_2020 = from_2020.sort_values(by=['consommation_brute_totale'], ascending=False)
from_2020 = from_2020.dropna(subset=['consommation_brute_totale'])

from_2020_top5 = from_2020.head(5)
print(from_2020_top5)

# Use seaborn to barplot
sns.barplot(from_2020_top5['region'].values, from_2020_top5['consommation_brute_totale'])
plt.show()
