import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

path_db = "../data/db/consommation-annuelle-a-la-maille-adresse-2018-et-2019.csv"
df = pd.read_csv(path_db, sep=";")
col_list = df.columns.values.tolist()
at_least_one_dlp = df.dropna(subset=[df.columns[12], df.columns[14], df.columns[16]], thresh=1)
at_least_two_dlp = df.dropna(subset=[df.columns[12], df.columns[14], df.columns[16]], thresh=2)
#en absicce le numero d'entree (row) ou alors code_insee_commune
#en ordonee les columns 11 13 15
at_least_two_dlp.plot.bar(x=col_list[9], y={col_list[12], col_list[14], col_list[16]})
plt.xlabel('Communes')
plt.ylabel('Consommation de ressources')

print(df.iloc[1][9])



plt.show()
